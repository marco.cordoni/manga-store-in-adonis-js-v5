import Manga from 'App/Models/Manga'
import MangaService from 'App/Services/MangaService'
import { StatusCodes } from 'http-status-codes'
import { schema } from '@ioc:Adonis/Core/Validator'
import MangaNotFoundException from 'App/Exceptions/MangaNotFoundException'

export default class MangaController {
  private mangaService = new MangaService()

  // API

  /**
   * Get all manga
   */
  public async getAll({ response }) {
    return response.status(StatusCodes.OK).send(await this.mangaService.getAll())
  }

  /**
   * Get a manga by its Id
   */
  public async findById({ params, response }) {
    const { id } = params
    return response.status(StatusCodes.OK).send(await this.mangaService.findById(id))
  }

  /**
   * Save a manga
   */
  public async saveManga({ request, response }) {
    const newManga = request.requestBody
    const { name, authorName } = newManga

    return await this.mangaService
      .existByNameAndAuthor(name, authorName)
      .then(async (alreadyExists) => {
        if (alreadyExists) {
          return response.status(StatusCodes.UNPROCESSABLE_ENTITY).send('Manga already exists')
        } else {
          return response
            .status(StatusCodes.CREATED)
            .send(await this.mangaService.saveManga(newManga))
        }
      })
  }

  /**
   * Delete a manga by its Id
   */
  public async deleteById({ params, response }) {
    const { id } = params

    return await this.mangaService.findById(id).then(async (res) => {
      if (res) {
        await this.mangaService.deleteById(id)
        return response.status(StatusCodes.OK).send('Manga deleted')
      } else {
        throw new MangaNotFoundException()
      }
    })
  }

  /**
   * Delete a manga by its Id checking the authorization
   */
  public async deleteByIdWithAuthorization({ bouncer, params, response }) {
    console.log('DELETE WITH AUTH, PARAMS:', params)

    const { id, isAdmin } = params

    await bouncer.authorize('deleteManga', isAdmin)

    return await this.mangaService.findById(id).then(async (res) => {
      if (res) {
        await this.mangaService.deleteById(id)
        return response.status(StatusCodes.OK).send('Manga deleted')
      } else {
        throw new MangaNotFoundException()
      }
    })
  }

  /**
   * Update a manga by its Id
   */
  public async update({ request, response }) {
    const updateMangaSchema = schema.create({
      id: schema.number(),
      name: schema.string.optional({ trim: true }),
      genre: schema.string.optional({ trim: true }),
      authorName: schema.string.optional({ trim: true }),
    })

    const mangaToUpdate: Manga = await request.validate({ schema: updateMangaSchema })
    const { id } = mangaToUpdate

    return await this.mangaService.findById(id).then(async (res) => {
      if (res) {
        return response.status(StatusCodes.OK).send(await this.mangaService.update(mangaToUpdate))
      } else {
        throw new MangaNotFoundException()
      }
    })
  }

  // VIEWS

  /**
   * Get all manga and show in view
   */
  public async getAllView({ view }) {
    const mangaList = await this.mangaService.getAll()

    const html = await view.render('home', {
      greeting: 'Hello',
      mangaList: mangaList.length > 0 ? mangaList : null,
      // deleteByIdFunc: this.mangaService.deleteById,
    })

    return html
  }
}
