import Manga from 'App/Models/Manga'
import MangaRepository from 'App/Repositories/MangaRepository'

export default class MangaService {
  private mangaRepository = new MangaRepository()

  /**
   * Get all manga
   */
  public getAll(): Promise<Manga[]> {
    return Manga.all()
  }

  /**
   * Get a manga by its Id
   */
  public async findById(id: number): Promise<Manga | null> {
    return Manga.find(id)
  }

  /**
   * Count the manga filtered by name and author
   */
  public async existByNameAndAuthor(name: string, authorName: string): Promise<any> {
    return await this.mangaRepository.countMangaByNameAndAuthor(name, authorName).then((res) => res)
  }

  /**
   * Save a manga
   */
  public saveManga(newManga: Manga): Promise<Manga> {
    return Manga.create(newManga)
  }

  /**
   * Delete a manga by its Id
   */
  public async deleteById(id: number) {
    const mangaToDelete = await Manga.find(id)
    await mangaToDelete?.delete()
  }

  /**
   * Update a manga by its Id
   */
  public async update(mangaNew: Manga) {
    const mangaOld = await Manga.find(mangaNew.id)

    if (mangaOld) {
      return await mangaOld.merge(mangaNew).save()
    }
  }
}
