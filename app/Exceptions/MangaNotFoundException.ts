import { Exception } from '@adonisjs/core/build/standalone'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

/*
|--------------------------------------------------------------------------
| Exception
|--------------------------------------------------------------------------
|
| The Exception class imported from `@adonisjs/core` allows defining
| a status code and error code for every exception.
|
| @example
| new MangaNotFoundException('message', 500, 'E_RUNTIME_EXCEPTION')
|
*/
export default class MangaNotFoundException extends Exception {
  constructor() {
    const message = 'Manga not found'
    const status = 404
    const errorCode = 'E_RESOURCE_NOT_FOUND'

    super(message, status, errorCode)
  }

  public async handle(error: this, ctx: HttpContextContract) {
    ctx.response.status(error.status).send(error.message)
  }
}
