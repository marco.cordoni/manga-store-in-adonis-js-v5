import Manga from 'App/Models/Manga'
import { Repository } from './Repository'

export default class MangaRepository extends Repository<typeof Manga> {
  public model = Manga

  public async countMangaByNameAndAuthor(name: string, authorName: string) {
    const countQuery = await this.model
      .query()
      .where('name', '=', name)
      .where('authorName', '=', authorName)
      .count('*', 'total')
    return countQuery[0].$extras.total > 0 ? true : false
  }
}
