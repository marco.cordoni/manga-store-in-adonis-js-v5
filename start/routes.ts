/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  Route.get('getAll', 'MangaController.getAll')
  Route.get('findById/:id', 'MangaController.findById')
  Route.post('save', 'MangaController.saveManga')
  Route.delete('deleteById/:id', 'MangaController.deleteById')
  Route.delete(
    'deleteByIdWithAuthorization/:id/:isAdmin',
    'MangaController.deleteByIdWithAuthorization'
  )
  Route.put('update', 'MangaController.update')
}).prefix('/manga')

Route.group(() => {
  Route.get('getAll', 'MangaController.getAllView')
}).prefix('/manga/view')

Route.group(() => {
  Route.get('health', 'UtilsController.health')
}).prefix('/utils')
